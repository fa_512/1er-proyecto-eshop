<?php
  session_start();

  $user = $_SESSION['user'];
  if (!$user) {
    header('Location: index.php');
  }
  ?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
 
 <h1> Bienvenido <?php echo $user['name']; echo $user['lastname'] ?> </h1>
  <!--<a href="logout.php">Logout</a>-->


 <!-- <nav class="nav">
    <?php  if($user['usertype'] === 'admin') { ?>
      <li class="nav-item">
        <a class="nav-link active" href="#">Cantidad de clientes Registrados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="#">Cantidad de productos vendidos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="#">Monto total de ventas</a>
      </li>
     
    <?php } ?>
    <li class="nav-item">
      <a class="nav-link disabled" href="categorias.php" tabindex="-1" aria-disabled="true">categorias</a>
    </li>
  </nav>-->

  <nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item">
      Cantidad de clientes Registrados
      </a>

      <a class="navbar-item">
      Cantidad de productos vendidos
      </a>
      <a class="navbar-item">
      Monto total de ventas
      </a>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
        Administrar
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item" href="list.php">
            Categorias
          </a>
          <a class="navbar-item">
            Productos
          
        </div>
      </div>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <!--<a class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a class="button is-light">
            Log in
          </a>-->
          <a class="button is-light" href="logout.php">
            Logout
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>
