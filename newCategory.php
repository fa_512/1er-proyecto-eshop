<?php
$message = "";
if (!empty($_REQUEST['status'])) {
  $message = $_REQUEST['message'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">

  <title>Document</title>
</head>

<body>
  <div class="container">
    <div class="columns">
      <?php echo $message; ?>
    </div>
    <h1 class="title">Form Registration Category</h1>
    <form action="saveCategory.php" method="POST" class="form-inline" role="form">

      <div class="field">
        <div class="control">
          <label class="label" for="">Name</label>
          <input type="text" class="input is-success" name="name" placeholder="Your Name">
        </div>
      </div>
      <div class="field">
        <div class="control">
          <label class="label" for="">Description</label>
          <input type="text" class="input is_success" name="description" placeholder="Your Description">
        </div>
      </div>

      <button  class="button is-primary" value="Submit">Create</input>
    </form>
  </div>

</body>

</html>