  <?php
  // get all users from the database
  $sql = 'SELECT * FROM categoria';
  $connection = new mysqli('localhost:3306', 'root', '12345', 'eshop');
  $result = $connection->query($sql);
  $categorys = $result->fetch_all();
  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">

    <title>Document</title>
  </head>

  <body>
    <div class="container">
      <h1>List of Category</h1>
      <a class="button is-light" href="newCategory.php">
            New Category
          </a>
      <table class="table ">
        <thead>
          <tr>
            <th>Name</th>
            <th>description</th>
            <th>Actions</th>
          </tr>
        <tbody>
          <?php
          // loop users
          foreach ($categorys as $category) {
            echo "<tr class='is-selected'><td>" . $category[0] . "</td><td>" . $category[1] . "</td><td><a href=\"editcategory.php" . $category[1] . "\">Edit</a> |
              <a href='delete.php?description=" . $category[1] . "'>delete</a> </td></tr>";
          }
          ?>
        </tbody>
        </thead>

      </table>
      <?php

      $connection->close();
      ?>
    </div>
  </body>

  </html>